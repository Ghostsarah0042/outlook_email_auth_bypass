# Summary
    
In Outlook desktop and web app , "display name" of email's "From" header can manipulate the from email which is displayed to the user, that can result in more convincing phish emails 
    
# Outlook Desktop

### Description
    
Two bugs in outlook makes it possible for the attacker to make the email to appear to be coming from an arbitrary email address or even no email (which in outlook signifies that it is from the same organization):

1. display name and from email are in same visual element , there is no isolation between them, thus an authenticated piece of information(from email) can be manipulated by  an arbitrary attacker controlled string (display name)

2. Instead of letting user  know that "display name"+"from email" is very long in some way it is quietly truncated.

Thus Attacker can simply push the "from email" out of the screen.
This is as severe as a browser bug that lets websites choose what they want to show in address bar.

    
### POC

IMP NOTE: most email clients will manipulate common whitespaces before creating an email, so use something like ssmtp that lets you send email as raw to properly control what exactly is being sent.


this is how a typical mail coming from outside of the organization looks, clearly showing the from address.
![](desktop/3.png)

we just need to add lots of spaces  in the display name you want to show:

`Your Manager[SPACE][SPACE]........` gives us
![](desktop/4.png)

If you want to show a fake email then just append that in the display name:

`Your Manager <your.manager@company.com>[SPACE][SPACE].....` gives us
![](desktop/5.png)

#### Perfecting the exploit

The above two looks just like legit emails, but there is one small anomaly in the preview pane, display name has "..." or our fake email address at the end, that might alert  few people, we can use yet another trick:
* [TAB] are not compacted in preview pane but are in the message viewer.

`Your Manager[TAB][TAB]....[TAB]<your.manager@company.com>[SPACE][SPACE].....` gives us 
![](desktop/20.png)
and,  

`Your Manager[TAB][TAB]....[TAB][SPACE][SPACE].....` gives us
![](desktop/19.png)

Now they are equivalent to legit mails in all aspects.
    
# Outlook web

### Description

The following bug in outlook makes it possible for the attacker to make the email to appear to be coming from an arbitrary email address or even no email (which in outlook signifies that it is from the same organization):

* display name and from email are in same visual element , there is no isolation between them, thus an authenticated piece of information(from email) can be manipulated by  an arbitrary attacker controlled string (display name)

Exploiting this bug needs us to be a bit creative as we can't force outlook to not display the real from address like we did in outlook desktop.

    
### POC

IMP NOTE: most email clients will alter common whitespaces before creating an email, so use something like ssmtp that lets you send email as raw to properly control what exactly is being sent.

Note: In the following exploit snippets i m using "_" to represent any blank appearing character that is not treated as a whitspace by html , i specifically used BRAILLE PATTERN BLANK U+2800

this is how a typical mail coming from outside of the organization looks, clearly showing the from address.
![](web/6.png)

Trick is that even if we can't make the from address disappear, we can change how the recipient interprets it. we will make the from address to appear as if it is one of the recipients in TO:

For doing that we need  to suppress the real To: line that outlook produces, that can be achieved by send the mail with BCC: and not with TO: header.
Outlook simply appends the from address after the display name.
so we can provide a display name such as:

`Your[SPACE]Manager[SPACE]______________________[SPACE]To:_colleague1<collegue1@company.com>;_ colleague1<collegue1@company.com>;_colleague3`

that gives us (on small screen):
![](web/7_small.png)
and  (on big screen)
![](web/7_big.png)

Lets understand how this exploit worked on two different screen sizes:
* HTML engine compacts the normal whitespaces so they can't be used for making the To: line appear on next line, so i have used BRAILLE PATTERN BLANK.
* We can't know the screen size of recipient so we need to craft our display name in such a way that HTML breaks at the start of To: only.
* To do that we need to make the To: line appear as one big word.
* Then  the attacker needs to choose a MIN and MAX size of the screen that he wants to attack.
* lets say len is the size of full display name string and to_len is the size of "To:" string. we need to make the display string such that: len > MAX and len-to_len < MIN
* Thus by choosing the length of "__________________" before the To: string and the length of To: string we can make the exploit reliably work on screens of many sizes.  
* In simpler words if the display name was allowed to overflow out of the screen we need to make the end of the screen intersect the To: line.

Similarly an fake email address  can be shown by appending it after the "Your Manager" , gives us 
![](web/8.png)

we can also make some space between the display name line and the To: line like
![](web/12.png)

With the power unicode the possibilities of even better exploits is always there.

### Limitations
* real To: line has slightly smaller font size then our fake one,but  we can can't rely  upon users to tell them apart.
* there is one small anomaly in the preview pane, display name has "..." or our fake email address at the end, again not likely to alarm many people.


## Note
All [NUM].png have corresponding exploit_[NUM].txt which contains the display name that can produce the result.
